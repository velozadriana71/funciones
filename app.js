const startGameBtn = document.getElementById("start-game-btn");

const ROCK = "ROCK";
const PAPER = "PAPER";
const SCISSORS = "SCISSORS";
const DEFAULT_USER_CHOICE = ROCK;
const RESULT_DRAW = "DRAW";
const PLAYER_WIN = "PLAYER_WIN";
const COMPUTER_WIN = "COMPUTER_WIN";

let gameIsRunning = false;

const getPlayerChoice = function () {
  const selection = prompt(
    `${ROCK}, ${PAPER} or ${SCISSORS}?`,
    ""
  ).toUpperCase();

  if (selection !== ROCK && selection !== PAPER && selection !== SCISSORS) {
    alert(`Invalid choice! Whe choice ${DEFAULT_USER_CHOICE} for you!`);
    return DEFAULT_USER_CHOICE;
  }
  return selection;
};

const getComputerChoice = function () {
  const randomValue = Math.random();
  if (randomValue < 0.34) {
    return ROCK;
  } else if (randomValue < 0.67) {
    return PAPER;
  } else {
    return SCISSORS;
  }
};

const getWinner = (cChoice, pChoice) => {
  return cChoice === pChoice
    ? RESULT_DRAW
    : (cChoice === ROCK && pChoice === PAPER) ||
      (cChoice === PAPER && pChoice === SCISSORS) ||
      (cChoice === SCISSORS && pChoice === ROCK)
    ? PLAYER_WIN
    : COMPUTER_WIN;

//   if (cChoice === pChoice) {
//     return RESULT_DRAW;
//   } else if (
//     (cChoice === ROCK && pChoice === PAPER) ||
//     (cChoice === PAPER && pChoice === SCISSORS) ||
//     (cChoice === SCISSORS && pChoice === ROCK)
//   ) {
//     return PLAYER_WIN;
//   } else {
//     return COMPUTER_WIN;
//   }
};

startGameBtn.addEventListener("click", function startGame() {
  if (gameIsRunning) {
    return;
  }
  gameIsRunning = true;
  console.log("Game is starting...");
  const playerSelection = getPlayerChoice();
  const computerSelection = getComputerChoice();
  const winner = getWinner(computerSelection, playerSelection);
  let message = `You picked ${playerSelection}, computer picked ${computerSelection}, therefore you `;
  if(winner === RESULT_DRAW){
    message = message + 'had a draw.'
  } else if (winner === PLAYER_WIN) {
    message = message + 'won.'
  } else {
    message = message + 'lost.'
  }
  alert(message);
  gameIsRunning = false;
});

//NOT RELATED TO GAME
const sumUp = () => {
  
}

//Function anonima
// const start = function() {
//     console.log("Game is starting...");
// }

// function startGame() {
//   console.log("Game is starting...");
// }

// const person = {
//   greet: function greet() {
//     console.log("Hello there");
//   },
// };

// person.greet();

// startGame();
